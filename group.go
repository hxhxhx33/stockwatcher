package main

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/golang/glog"
)

func groupHandler(w http.ResponseWriter, r *http.Request) {
	glog.Infof("[group] %s %s\n", r.Method, r.URL.Path)

	switch r.Method {
	case http.MethodGet:
		if ms := groupPtn.FindStringSubmatch(r.URL.Path); len(ms) == 2 {
			group(ms[1], w, r)
			return
		}
		notFound(w)
	case http.MethodPost:
		if ms := createGroupPtn.FindStringSubmatch(r.URL.Path); len(ms) == 1 {
			createGroup(w, r)
			return
		}
		if ms := createStockPtn.FindStringSubmatch(r.URL.Path); len(ms) == 2 {
			createStock(ms[1], w, r)
			return
		}
		notFound(w)
	default:
		notFound(w)
	}
}

//group
var groupPtn = regexp.MustCompile("^/group/(.+?)/?$")

type groupData struct {
	Name   string
	Stocks []stock
}

func group(name string, w http.ResponseWriter, r *http.Request) {
	db, err := loadDatabase()
	if err != nil {
		internalError(w, err)
		return
	}

	var g stockGroup
	for _, t := range db.StockGroups {
		if t.Name == name {
			g = t
			break
		}
	}
	if g.Name == "" {
		notFound(w)
		return
	}

	var data groupData
	data.Name = g.Name
	data.Stocks = g.Stocks

	renderPage(groupTemplate, data, w)
}

//createGroup
var createGroupPtn = regexp.MustCompile("^/groups/?$")

func createGroup(w http.ResponseWriter, r *http.Request) {
	db, err := loadDatabase()
	if err != nil {
		internalError(w, err)
		return
	}

	form, err := parseGroupForm(r)
	if err != nil {
		internalError(w, err)
		return
	}

	if err := form.validate(); err != nil {
		groupFormFailed(w, r, form, err)
		return
	}

	for _, g := range db.StockGroups {
		if g.Name == form.Name {
			groupFormFailed(w, r, form, fmt.Errorf("%s 已经存在", form.Name))
			return
		}
	}

	g := stockGroup{
		Name: form.Name,
	}
	db.StockGroups = append(db.StockGroups, g)
	if err = db.save(); err != nil {
		internalError(w, err)
		return
	}

	// return
	data := groupFormData{
		Form: form,
	}

	respondJSON(w, data)
}

//createStock
var createStockPtn = regexp.MustCompile("^/group/(.+?)/stocks/?$")

func createStock(gname string, w http.ResponseWriter, r *http.Request) {
	db, err := loadDatabase()
	if err != nil {
		internalError(w, err)
		return
	}

	gidx := -1
	for i, g := range db.StockGroups {
		if g.Name == gname {
			gidx = i
			break
		}
	}

	if gidx < 0 {
		notFound(w)
		return
	}

	form, err := parseStockForm(r)
	if err != nil {
		internalError(w, err)
		return
	}

	if err := form.validate(); err != nil {
		stockFormFailed(w, r, form, err)
		return
	}

	s := stock{
		Code: form.Code,
		Name: form.Name,
	}

	// prepend
	stocks := db.StockGroups[gidx].Stocks

	// check duplication
	for _, st := range stocks {
		if st.Code == s.Code {
			stockFormFailed(w, r, form, fmt.Errorf("代码 %s 已经存在", s.Code))
			return
		}
	}

	// prepend
	stocks = append([]stock{s}, stocks...)
	db.StockGroups[gidx].Stocks = stocks

	if err = db.save(); err != nil {
		internalError(w, err)
		return
	}

	// return
	data := stockFormData{
		Form: form,
	}

	respondJSON(w, data)
}
