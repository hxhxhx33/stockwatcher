package main

import (
	"html/template"

	"github.com/golang/glog"
)

var indexTemplate *template.Template
var groupTemplate *template.Template

func mustPrepareTemplates() {
	indexTemplate = mustParseTemplateLayout("index.html")
	groupTemplate = mustParseTemplateLayout("group.html")
	return
}

func mustParseTemplateLayout(fileNames ...string) (t *template.Template) {
	fileNames = append(fileNames, "layout.html", "base.html")
	return mustParseTemplate(fileNames...)
}

func mustParseTemplate(fileNames ...string) (t *template.Template) {
	t = template.New("")

	t = t.Funcs(template.FuncMap{
		"add": addFn,
	})

	for _, fileName := range fileNames {
		text, e := bundle.FindString(fileName)
		if e != nil {
			glog.Fatal(e)
		}

		t, e = t.Parse(text)
		if e != nil {
			glog.Fatal(e)
		}
	}

	return
}

var addFn = func(vs ...int) int {
	var s int
	for _, v := range vs {
		s += v
	}
	return s
}
