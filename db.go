package main

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// For such toy demo, we use file system as a database.

type stock struct {
	Name string `yaml:"name"`
	Code string `yaml:"code"`
}

type stockGroup struct {
	Name   string  `yaml:"name"`
	Stocks []stock `yaml:"stocks"`
}

type database struct {
	StockGroups []stockGroup `yaml:"stock_groups"`
}

func loadDatabase() (db database, err error) {
	dbPath := webConf.Database

	content, err := ioutil.ReadFile(dbPath)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(content, &db)

	return
}

func (db *database) save() (err error) {
	bytes, err := yaml.Marshal(db)
	if err != nil {
		return
	}

	savePath := webConf.Database
	if err = ioutil.WriteFile(savePath, bytes, 0644); err != nil {
		return
	}

	return
}
