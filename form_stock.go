package main

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

type stockForm struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

type stockFormData struct {
	Form  stockForm `json:"form"`
	Error string    `json:"error"`
}

func stockFormFailed(w http.ResponseWriter, r *http.Request, form stockForm, err error) {
	data := stockFormData{
		Form:  form,
		Error: err.Error(),
	}
	respondJSON(w, data)
}

var stockCodePtn = regexp.MustCompile(`^[A-Z0-9a-z\.]+$`)

func (f *stockForm) validate() (err error) {
	if f.Name == "" {
		err = fmt.Errorf("名称不能为空")
		return
	}
	if f.Code == "" {
		err = fmt.Errorf("代码不能为空")
		return
	}
	if !stockCodePtn.MatchString(f.Code) {
		err = fmt.Errorf("股票代码必须为字母与数字或小数点")
		return
	}
	f.Code = strings.ToUpper(f.Code)
	return
}

func parseStockForm(r *http.Request) (form stockForm, err error) {
	if err = r.ParseForm(); err != nil {
		return
	}

	form.Name = r.FormValue("name")
	form.Code = r.FormValue("code")

	return
}
