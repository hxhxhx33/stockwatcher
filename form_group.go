package main

import (
	"fmt"
	"net/http"
)

type groupForm struct {
	Name string `json:"name"`
}

type groupFormData struct {
	Form  groupForm `json:"form"`
	Error string    `json:"error"`
}

func groupFormFailed(w http.ResponseWriter, r *http.Request, form groupForm, err error) {
	data := groupFormData{
		Form:  form,
		Error: err.Error(),
	}
	respondJSON(w, data)
}

func (f *groupForm) validate() (err error) {
	if f.Name == "" {
		err = fmt.Errorf("名称不能为空")
		return
	}
	return
}

func parseGroupForm(r *http.Request) (form groupForm, err error) {
	if err = r.ParseForm(); err != nil {
		return
	}

	form.Name = r.FormValue("name")

	return
}
