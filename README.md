# Prerequisite

You need to [install GoLang](https://golang.org/dl/) to run the demo.

# Run

```
make run
```

# Build

For linux, run

```
make linux
```

which will create a `stockwatcher_linux_amd64` binary.

For mac, run

```
make mac
```

which will create a `stockwatcher_mac_amd64` binary.

# Deploy

Simply copy the built binary to the host.
