package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gobuffalo/packr"
	"github.com/golang/glog"
)

var webConf = newConf()
var bundle packr.Box

func main() {
	port := flag.Int("p", 3001, "port")
	confPath := flag.String("c", "", "path to the conf file")
	flag.Parse()

	// Load conf
	if *confPath != "" {
		err := webConf.load(*confPath)
		if err != nil {
			glog.Fatal(err)
		}
	}

	// initalize db if necessasry
	if err := initDBIfNotExist(webConf.Database); err != nil {
		glog.Fatal(err)
		return
	}

	// Bundle
	bundle = packr.NewBox("./view")

	//Prepare templates
	mustPrepareTemplates()

	// Serve static files
	staticBox := packr.NewBox("./static")
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(staticBox)))

	// Serve index
	http.HandleFunc("/", indexHandler)

	// Serve group
	http.HandleFunc("/group/", groupHandler)
	http.HandleFunc("/groups/", groupHandler)

	// Start
	fmt.Printf("Listenning on :%d\n", *port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)
	if err != nil {
		glog.Fatal(err)
	}
}
