package main

import (
	"net/http"
	"regexp"

	"github.com/golang/glog"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	glog.Infof("[index] %s %s\n", r.Method, r.URL.Path)

	switch r.Method {
	case http.MethodGet:
		if ms := indexPtn.FindStringSubmatch(r.URL.Path); len(ms) == 1 {
			index(w, r)
			return
		}
		notFound(w)
	default:
		notFound(w)
	}
}

//index
var indexPtn = regexp.MustCompile("^/?$")

type indexData struct {
}

func index(w http.ResponseWriter, r *http.Request) {
	renderPage(indexTemplate, nil, w)
}
