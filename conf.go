package main

import (
	"fmt"
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

//Conf ...
type Conf struct {
	Title    string `yaml:"title"`
	Database string `yaml:"database"`
}

func newConf() Conf {
	var c Conf
	c.Title = "Stock Watcher"
	c.Database = "db.yaml"
	return c
}

func (c *Conf) load(confPath string) (err error) {
	bytes, err := ioutil.ReadFile(confPath)
	if err != nil {
		return
	}
	if err = yaml.Unmarshal(bytes, c); err != nil {
		return
	}

	if err = c.validate(); err != nil {
		return
	}

	return
}

func (c *Conf) validate() error {
	// set some default values
	if c.Title == "" {
		return fmt.Errorf("missing title")
	}
	if c.Database == "" {
		return fmt.Errorf("missing database")
	}

	return nil
}

func initDBIfNotExist(dbPath string) (err error) {
	if _, e := os.Stat(dbPath); !os.IsNotExist(e) {
		return
	}

	// an empty database
	var db database

	return db.save()
}
