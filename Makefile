BIN_NAME=stockwatcher

usage:
	@echo "make (run|linux|mac)"

run:
	go run *.go

linux:
	GOOS=linux GOARCH=amd64 packr build -o $(BIN_NAME)_linux_amd64

mac:
	GOOS=darwin GOARCH=amd64 packr build -o $(BIN_NAME)_darwin_amd64
