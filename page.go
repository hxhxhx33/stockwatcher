package main

import (
	"html/template"
	"net/http"
)

type page struct {
	Title       string
	Data        interface{}
	StockGroups []string
}

func renderPage(t *template.Template, data interface{}, w http.ResponseWriter) {
	var p page
	p.Data = data
	p.Title = webConf.Title

	db, err := loadDatabase()
	if err != nil {
		internalError(w, err)
		return
	}
	for _, sg := range db.StockGroups {
		p.StockGroups = append(p.StockGroups, sg.Name)
	}

	if err := t.Execute(w, p); err != nil {
		internalError(w, err)
		return
	}
}
